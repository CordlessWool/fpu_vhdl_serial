----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2017 14:13:45
-- Design Name: 
-- Module Name: FPU_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FPU_sim is
--  Port ( );
end FPU_sim;

architecture Behavioral of FPU_sim is

    signal clk: std_logic_vector(31 downto 0);
    signal number1: std_logic_vector(31 downto 0) := ;
    signal number2: std_logic_vector(31 downto 0);
    signal operation: std_logic_vector(1 downto 0)
    signal result: std_logic_vector(31 downto 0));

    component FPU
    Port (clk: in STD_Logic; 
            number1: in std_logic_vector(31 downto 0);
            number2: in std_logic_vector(31 downto 0);
            operation: in std_logic_vector(1 downto 0)
            result: out std_logic_vector(31 downto 0));
    end component;
        
    )

begin

test: process
begin
    
end process test;

c1: FPU port map (clk, number1, number2, operation, result)

end Behavioral;
