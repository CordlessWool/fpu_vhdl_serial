----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2017 14:11:09
-- Design Name: 
-- Module Name: FPU - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FPU is
  Port (clk: in STD_Logic; 
        number1: in std_logic_vector(31 downto 0);
        number2: in std_logic_vector(31 downto 0);
        operation: in std_logic_vector(1 downto 0)
        result: out std_logic_vector(31 downto 0));
end FPU;

architecture Behavioral of FPU is

begin

shift: process(clk)
    begin
        out <= ('0'+number1)+ ('0'+number2);
    end proess shift;

end Behavioral;
